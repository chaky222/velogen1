var marker = false;
var MARKERS_IMAGE_DIR = "/wthemes/velo/theme/velo/maps/images/markers";
var MIN_ZOOM_FOR_ICONS_DRAW = 14;


var markerInfo = document.getElementById('mark-info');
var infowindow;
// $('<div/>').text('This is fun & stuff').html();
// var region_markers = [];
// var cities_markers = {};
// var single_markers = {};

var mapTypes={
  all : 0,
  pub : 1,
  my  : 2
};

var icons2={
  907 : 'parking.png',
  918 : 'veloshop.png',
  920 : 'veloprokat.png',
  921 : 'velopark.png',
  
  922 : 'tourism.png',
  924 : 'tourism.png',
  923 : 'tourism.png',
  925 : 'tourism.png',
  
  908 : 'home.png',
  911 : 'home.png',
  940 : 'home.png',
  941 : 'home.png',
  917 : 'home.png',
  942 : 'home.png',
  943 : 'home.png',
  
  909 : 'eat.png',  
  913 : 'eat.png',  
  914 : 'eat.png',  
  944 : 'eat.png',  
  945 : 'eat.png',  
  946 : 'eat.png',  
  947 : 'eat.png',  
  
  927 : 'shop.png',
  953 : 'shop.png',
  948 : 'shop.png',
  949 : 'shop.png',
  950 : 'shop.png',
  951 : 'shop.png',
  952 : 'shop.png',
  954 : 'shop.png',
  
  928 : 'medic.png',
  955 : 'medic.png',
  956 : 'medic.png',
  957 : 'medic.png',
  958 : 'medic.png',
  959 : 'medic.png',
  
  929 : 'wifi.png',
  960 : 'wifi.png',
  961 : 'wifi.png',
  
  930 : 'bank.png',
  977 : 'bank.png',
  978 : 'bank.png',
  
  931 : 'museum.png',
  938 : 'museum.png',
  939 : 'museum.png',
  979 : 'museum.png',
  980 : 'museum.png',
  981 : 'museum.png',
  982 : 'museum.png',
  984 : 'museum.png',
  
  932 : 'party.png',
  988 : 'party.png',
  989 : 'party.png',
  990 : 'party.png',
  991 : 'party.png',
  992 : 'party.png',
  
  933 : 'campingzone.png',
  983 : 'campingzone.png',
  986 : 'campingzone.png',
  987 : 'campingzone.png',
  
  934 : 'autobus.png',
  993 : 'autobus.png',
  994 : 'autobus.png',
  1008 : 'autobus.png',
  
  964 : 'postcouritr.png',
  965 : 'postcouritr.png',
  966 : 'postcouritr.png',
  967 : 'postcouritr.png',
  963 : 'postcouritr.png',
  968 : 'postcouritr.png',
  969 : 'postcouritr.png',
  970 : 'postcouritr.png',
  971 : 'postcouritr.png',
  972 : 'postcouritr.png',
  973 : 'postcouritr.png',
  974 : 'postcouritr.png',
  975 : 'postcouritr.png',
  976 : 'postcouritr.png',
  
  935 : 'auto.png',
  995 : 'auto.png',
  996 : 'auto.png',
  1007 : 'auto.png',
  
  936 : 'uslugi.png',
  962 : 'uslugi.png',
  997 : 'uslugi.png',
  
  937 : 'city.png',
  998 : 'city.png',
  999 : 'city.png',
  1000 : 'city.png',
  1001 : 'city.png',
  1002 : 'city.png',
  1003 : 'city.png',
  1004 : 'city.png',
  1005 : 'city.png',
  1006 : 'city.png'
};

var detail_icons = false;

var XHR;

var map,
    geocoder = new google.maps.Geocoder();



var mainMark_select  = document.getElementById('mark-main-type');
var childMark_select = document.getElementsByClassName('mark-child-type');

var markBaseType_value = false,
    markChildType_value = false;

Object.size = function(obj) {
    var size = 0;

    for (var key in obj)
        if (obj.hasOwnProperty(key)) size++;

    return size;
};


function baseType_select() {
    if(this.selectedIndex) {
        markBaseType_value = this.value;
        markChildType_value = false;

        for(var i = 0; i < childMark_select.length; i++) {
            if('parent-' + this.value == childMark_select[i].getAttribute('id')) {
                markChildType_value = childMark_select[i].value;

                childMark_select[i].style.display = 'block';
                childMark_select[i].setAttribute('class', 'mark-child-type active');

                childMark_select[i].removeEventListener('change', childType);
                childMark_select[i].addEventListener('change', childType);
            } else {
                childMark_select[i].style.display = 'none';
                childMark_select[i].setAttribute('class', 'mark-child-type');
            }
        }
    } else {
        for(var i = 0; i < childMark_select.length; i++) {
            childMark_select[i].style.display = 'none';
            childMark_select[i].setAttribute('class', 'mark-child-type');
        }
    }
}

function geocodePosition(pos) {
    geocoder.geocode({
        latLng: pos
    }, function(responses) {
        console.log(responses);
        if (responses && responses.length > 0) {
            // response = responses[1];
        } else {
            console.log('Cannot determine address at this location.');
        }
    });
}
function map_drag_end(){
  var mark = getEditMark();
  console.log('map_drag_end');
  if (mark.inEditMode) {
    var lat = parseFloat(mark.mark_elem.position.lat());
    var lng = parseFloat(mark.mark_elem.position.lng());
    console.log('lat='+lat+' lng='+lng);
  }
}

 function testConvertTmp(){
  $.ajax({
      type: "GET",
      data: ""+"&random="+Math.random(),
      url: '/ru/velo_maps/',
      // type: json,
      success: function(msg){
        console.log(msg);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        
      }              
  });  
 }

function childType() {
    markChildType_value = this.value;
}

mainMark_select.addEventListener('change', baseType_select);



function toggle_dropdown(elem){
  var drop_up = (((+$('.section.maps-section').css('height'))) < (+$(elem).closest('.gm-style-iw').parent().offset().top + 400))? true : false;
  $dropdown = $(elem).closest('.dropdown');
  if (drop_up){
    $dropdown.not('.drop_up').addClass('drop_up');
  }else{
    $dropdown.removeClass('drop_up');
  }  
  $dropdown.children('dd').children('ul').toggle();
}

function dropdown_selected(elem){
  // console.log('clicked li='+$(elem).closest('li').data('value'));
  $(elem).closest('li').parent().children('li').each(function(){
    $(this).find('label.selected').removeClass('selected');
  });
  $(elem).not('.selected').addClass('selected');
  $dd = $(elem).closest('dl.dropdown');
  // var text = ;
  $dd.find("dt > label span").html($(elem).html());
  $dd.find("dd ul").hide();
  var new_val = (+$(elem).closest('li').data('value'));
  $dd.children('.dropdown_value').attr('value', new_val);
  var cur_level = (+$dd.attr('level'));
  if (new_val > 0){
    $dd.parent().find('.dropdown[level="'+(cur_level+1)+'"]').remove();
    var new_type = window.JS_ENV.data.markerTypes[new_val];        
    if (new_type.c.length > 0){
      var new_type_select = genSelectMarkerType(new_val, true, true);
      $(new_type_select).insertAfter($dd);
      $(elem).closest('.markInfo_EditMarkerData').find('.markNewTypeValue').attr('value', '0');
    }else{
      $(elem).closest('.markInfo_EditMarkerData').find('.markNewTypeValue').attr('value', new_val);
    }
  }else{
    $(elem).closest('.markInfo_EditMarkerData').find('.markNewTypeValue').attr('value', '0');
  }
}



function genSelectMarkerType(type_id,isItParentId,skipPrintParent, recursion){  
  recursion       = recursion       || 0;
  isItParentId    = isItParentId    || false;
  skipPrintParent = skipPrintParent || false;
  if (recursion > 10) return '';
  // console.log('genSelectMarkerType=');
  // console.log(type_id);
  var result = '';
  
  var init_selected = "<div class='mark_img'></div>Тип метки";
  var parent = 0;
  var level = 0;
  if (isItParentId){
    parent  = type_id;
    type_id = 0;
  }else{
    var type = window.JS_ENV.data.markerTypes[(+type_id)];
    if (type){
      level = type.l;
      parent = ((type.p) && (type.p > 0)) ? type.p : 0;
    }    
  }  
  for (var mtype_id in window.JS_ENV.data.markerTypes) { 
    mtype_id = (+mtype_id);
    var mtype = window.JS_ENV.data.markerTypes[mtype_id];
    if (mtype.p == parent){
      level = mtype.l;
      var name = (mtype.c.length > 0) ? mtype.n + '&nbsp;»' : mtype.n;
      var image = (icons2[mtype.id]) ? '<img onerror=\'this.style.display = "none"\' class="flag" src="'+MARKERS_IMAGE_DIR+'/'+icons2[mtype.id]+'" /> ' : '';
      var item_html = '<div class="mark_img">'+image+'</div><span>'+name+'</span>';
      var selected = '';
      if (mtype.id == type_id){
        init_selected = item_html;
        selected = "class='selected'";
      }        
      result += '<li data-value="'+mtype.id+'"><label '+selected+' onclick="dropdown_selected(this);">'+item_html+'</label></li>';
      // result += "<li "+selected+" data-value='"+mtype.id+"'>"+mtype.n+"</li>\n";
    }    
  }
  
  // console.log('offset='+$('.gm-style-iw').parent().offset().top + 'height='+$('.gm-style-iw').css('height')+' screen_height='+);
  // console.log();
  // result = "<li "+init_selected+" data-value='0'>Тип метки</li>\n"+result;
  var result2 = '<dl class="dropdown UL_SelectMarker" level="'+level+'" parent="'+parent+'"><input type="hidden" class="dropdown_value" value="'+type_id+'"><dt><label onclick="toggle_dropdown(this);"><span>'+init_selected+'</span></label></dt><dd><ul>'+result+'</ul></dd></dl>';
  if ((parent > 0)&&(!skipPrintParent)){
    result2 = genSelectMarkerType(parent,false,false,recursion+1) + result2;
  }
  return result2;
}


var EDIT_MARK = {  
  id: 0,
  inEditMode: true
};



function deleteMarker(id){
  id = (id == undefined) ?  (+$(markerInfo).find('input[name="mark_id"]').attr('value')) : id;
  var mark = getEditMark(id);
  if (confirm("Удалить метку с названием: "+mark.title+"?")){
    $.post('', {'ajax': true,'type':'deleteMarker','data' : JSON.stringify({marks:[{id:id}]}).replace(/\\"/g, "&quot;")}, function(res, textStatus) {
      console.log(res);
      if (res.result > 0){
        if (mark.mark_elem){
          mark.mark_elem.setMap(null);
          mark.mark_elem = undefined; 
        }
        for (zoom=0; zoom < CACHED_MARKERS.c.length; zoom++){
          if (CACHED_MARKERS.c[zoom][id]){
            CACHED_MARKERS.c[zoom][id] = undefined;
            var indx = CACHED_MARKERS.printedMarksIds[zoom].indexOf(id);
            if (indx >= 0){ 
              $(markerInfo).find('input[name="mark_id"]').attr('value',0);
              $(markerInfo).attr('mark_id',0);
              CACHED_MARKERS.ObjectsInfoBlock.del(id);
              CACHED_MARKERS.printedMarksIds[zoom].splice(indx, 1);
            }
          }
        }
      }else{
        if (res.msg.length > 0) alert(res.msg);
      }
    }, "json");
  }
}

function edit_marker(center_to_mark, skip_inEditMode, id){
  skip_inEditMode=skip_inEditMode||false;
  center_to_mark= center_to_mark || false;
  // var id = (+$(markerInfo).attr('mark_id'));
  id = (id == undefined) ?  (+$(markerInfo).find('input[name="mark_id"]').attr('value')) : id;
  console.log('edit_marker id='+id);
  // console.log(window.JS_ENV);
  // $(markerInfo).attr('mark_id',id);
  // $(markerInfo).find('input[name="mark_id"]').attr('value',id);
  
  // if (id == 0) map.setZoom(18); // Решили отменить приближать зум при установке метки.

  // if (id == 0) {
  //   center_to_mark = true;
  //   var lat = CACHED_MARKERS.pos.latLng;
  //   var center_pos = {lat: (lat[0]+lat[2])/2, lng: (lat[1]+lat[3])/2};
  //   EDIT_MARK.lat = center_pos.lat-0.0005;
  //   EDIT_MARK.lng = center_pos.lng;
  // }else{

  // }


  var mark = getEditMark(id);
  console.log(mark);

  if (!skip_inEditMode) mark.inEditMode = true;
  
  var latLng = {lat: mark.lat, lng: mark.lng};
  // map.setCenter(center_pos); 
  var p = {
      position: latLng,
      labelContent: '',
      optimized: false,
      map: map,
      width: 1,
      draggable: mark.inEditMode,
      // labelClass: 'markerLabels m1',
      animation: google.maps.Animation.BOUNCE,
      // icon: '/wthemes/velo/dekor/markers/20.png',
      // icon: '/wthemes/velo/theme/velo/maps/images/add-new-marker-select-trigger-icon.png',
      zindex: 1    
  };  


  
  if ((!(id > 0)) && (mark.inEditMode) ){
    
    p.animation = google.maps.Animation.DROP;
    p.animation_changed = function(){        
      var _this = this;

      if ((_this.getAnimation() == null) && (!(_this.animation_flag))) {
        console.log('run bounce animation');
        _this.animation_flag = 1;        
        editModeInfoWindow(false);  
        setTimeout(function () { 
          console.log('stop_animation');
          _this.setAnimation(null);
          _this.animation_flag = 2;
        }, 3000);
        _this.setAnimation(google.maps.Animation.BOUNCE); 
        
      }
    }
  }else{
    
  }


  if (mark.mark_elem){
    mark.mark_elem.setMap(null);
    mark.mark_elem = undefined; 
  }   
  // console.log(p);  
  mark.mark_elem = new MarkerWithLabel(p);
  // return;
  // mark.title     = (mark.title)? mark.title          : "Создание новой метки";
  // mark.shortDesc = (mark.shortDesc) ? mark.shortDesc : "Таскайте метку по карте и нажмите сохранить!";
  if ((!(id > 0)) && (mark.inEditMode) ){

  }else{
    if ((mark.inEditMode)&&(id > 0)){
      setTimeout(function () { 
        console.log('stop_animation2');        
        mark.mark_elem.setAnimation(null);
      }, 3000);
    }
    showMarkerInfoWindowById(id, center_to_mark);  
    
  }  
  google.maps.event.addListener(mark.mark_elem, 'click', function(){
    showMarkerInfoWindowById(id, false);
  });
  if (mark.inEditMode){
    google.maps.event.addListener(mark.mark_elem, 'dragend', function() {
      console.log('Drag ended');
      // geocodePosition(mark.mark_elem.getPosition());
    });
  }
  
      
}






function addNewMarkToTheMap(){


// document.getElementById('add-marker-btn').addEventListener('click', function(){ 
  var lat = CACHED_MARKERS.pos.latLng;
  if (infowindow) infowindow.close();
  var center_pos = {lat: (lat[0]+lat[2])/2, lng: (lat[1]+lat[3])/2};
  if (EDIT_MARK.mark_elem){
    EDIT_MARK.mark_elem.setMap(null);
    EDIT_MARK.mark_elem = undefined; 
  }
  var map_coef = parseInt($('.section.maps-section').css('height'))/(lat[0] - lat[2]); // находим кофициент соотношения пикселей к латлнг
  var dialog_height = 350 / map_coef; // находим высоту нашего диалогового окошечка в латлнг...
  EDIT_MARK.lat = center_pos.lat-dialog_height/2; // ну а тут вроде всё и так ясно...
  EDIT_MARK.lng = center_pos.lng;
  EDIT_MARK.unsaved = true;
  EDIT_MARK.title    = '';
  EDIT_MARK.shortDesc = '';
  EDIT_MARK.unsaved_title     = $(markerInfo).find('.markNewTitleVal').attr('placeholder');
  EDIT_MARK.unsaved_shortDesc = $(markerInfo).find('.markNewDescrVal').attr('placeholder');;
  // $(markerInfo).attr('mark_id','0');
  $(markerInfo).find('input[name="mark_id"]').attr('value',0);
  $(markerInfo).attr('mark_id',0);
  edit_marker(false, false,0);

    // var markName = document.getElementById('marker-name').value;
    // var markShortDesc = document.getElementById('marker-short-desc').value;

    // var XHR = new XMLHttpRequest();

    // XHR.open('POST', '/ru/velo_maps', true);

    // XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    // XHR.send('type=addNewMark&baseType=' + markBaseType_value + '&childType=' + markChildType_value + '&lat=' + lat + '&lng=' + lng + '&ajax=1&markName=' + markName + '&markShortDesc=' + markShortDesc);
}

function getMarkTypemat(mark, recache){
  recache = recache || false;
  if ((!((mark.tc) && (mark.tc > 0))) || (recache)){
    var max_c =  0; var max_fval = 0;
    for (var f_val in mark.t) {
      if (mark.t[f_val].c >= max_c){
        max_c = mark.t[f_val].c;
        max_fval = f_val;
      }
    }
    mark.tc = (max_fval > 0) ? (+max_fval) : 0;
  }
  // if ((mark)&&(mark.t)&&(Object.keys(mark.t).length > 0)) {
  //   var typemat = (+Object.keys(mark.t)[0]);
  //   return (typemat > 0) ? typemat : 0;
  // }
  return mark.tc;
}

function markSaveChanges(elem){
  // testConvertTmp();
  // return;
  var $d = $(elem).closest('.markInfo_EditMarkerData').parent();
  var mark_id = (+$d.find('input[name="mark_id"]').attr('value'));
  var new_typemat = (+$d.find('.markNewTypeValue').attr('value'));
  if (!(new_typemat>0)){
    alert('Не указан тип метки! Пожалуйста укажите тип метки для сохранения.');
    return;
  }
  var new_title = $('.gm-style-iw .markNewTitleVal').val();
  if (!(new_title.length>2)){
    alert('Не указано название метки или название слишком короткое.');
    return;
  }
  var mark = getEditMark(mark_id);
  console.log('typemat1='+getMarkTypemat(mark));
  if (mark.inEditMode) {
    mark.inEditMode = false;
    var oldTypemat = getMarkTypemat(mark);
    if (!(mark.t)) mark.t = {};
    if (mark.t[oldTypemat]) mark.t[oldTypemat].c = 0;
    mark.t[new_typemat] = {c:1};
    mark.lat = parseFloat(mark.mark_elem.position.lat());
    mark.lng = parseFloat(mark.mark_elem.position.lng());
    mark.title = new_title;
    mark.shortDesc = $('.gm-style-iw .markNewDescrVal').val();
    mark.unsaved_title    = 'Сохранение метки '+ mark.title;
    mark.unsaved_shortDesc= 'Ожидание ответа сервера...';
    mark.unsaved = true;   
    if (mark_id > 0) {
      showMarkerInfoWindow();
    }else{
      edit_marker(false, true);
    }
    var save_mark = {
      id        : (mark_id)? mark_id : 0,
      lat       : mark.lat,
      lng       : mark.lng,
      title     : mark.title,
      shortDesc : mark.shortDesc,
      typemat   : new_typemat,
      t         : {}
    };
    save_mark.t[new_typemat] = {'c':1};
    geocoder.geocode({ latLng: mark.mark_elem.getPosition() }, function(responses) {
      save_mark['geocoder'] = responses;
      $.post('', {'ajax': true,'type':'updateMarks','data' : JSON.stringify({marks:[save_mark]}).replace(/\\"/g, "&quot;")}, function(res, textStatus) {
        if (res.result == 1){ 
          if (res.markFullInfo){            
            if (save_mark.id == 0){
              save_mark.id = (+Object.keys(res.markFullInfo)[0]);              
            }
            if (mark.mark_elem){
              mark.mark_elem.setMap(null);
              mark.mark_elem = undefined; 
            }
            var new_mark = res.markFullInfo[Object.keys(res.markFullInfo)[0]];
            if (!(CACHED_MARKERS.c[3][(+new_mark.src)])){
              CACHED_MARKERS.c[3][(+new_mark.src)] = {
                id : save_mark.src
              };
            }
            mark.unsaved = false;
            var m1 = CACHED_MARKERS.c[3][(+new_mark.src)];
                m1.lat            = parseFloat(new_mark.lat);
                m1.lng            = parseFloat(new_mark.lng);
                m1.title          = new_mark.name;
                m1.e              = 1;
                m1.shortDesc      = new_mark.description;
                m1.moreDataLoaded = true;
                m1['icon']        = (+new_mark.typemat);
                m1.t              = {};
                m1.t[new_mark.typemat] = {c:1};
            drawMark(save_mark.id, m1, CACHED_MARKERS.getZoom2());
            $(markerInfo).attr('mark_id',save_mark.id);
            $(markerInfo).find('input[name="mark_id"]').attr('value',save_mark.id);
            showMarkerInfoWindow();
            if(CACHED_MARKERS.printedMarksIds[CACHED_MARKERS.getZoom2()].indexOf(save_mark.id) < 0){
              CACHED_MARKERS.printedMarksIds[CACHED_MARKERS.getZoom2()].push(save_mark.id); 
            }               
          }
        }
      }, "json");
    });
  }
}



function markSaveChangesDone(new_id,save_result){
  var old_mark = getEditMark(save_result.old_id);
  var new_mark = getEditMark(new_id);
  // var 
}

function getEditMark(id){
  id = (id==undefined) ? $(markerInfo).attr('mark_id') : id;  
  id = (+id);
  console.log('id='+id);
  return (id > 0) ? CACHED_MARKERS.c[3][id] : EDIT_MARK;  
}

function editModeInfoWindow(cancel_edit){
  cancel_edit = cancel_edit || false;
  console.log('editModeInfoWindow cancel_edit='+cancel_edit);
  var markId = (+$(markerInfo).attr('mark_id'));
  var mark = getEditMark();   
  if (cancel_edit && (markId == 0)){
    if (infowindow) infowindow.close();
    if (mark.mark_elem){
      mark.mark_elem.setMap(null);
      mark.mark_elem = undefined; 
    } 
    return;
  } 
  mark.inEditMode = !cancel_edit;
  showMarkerInfoWindow();
}

function showMarkerInfoWindow(){
  var markId = (+$(markerInfo).attr('mark_id'));  
  var mark = getEditMark(markId);
  if (!mark) return;
  if (!(mark.shortDesc)) mark.shortDesc = '';
  if (mark.inEditMode) {
    $(markerInfo).find('.markInfo_Header').html((markId > 0) ? 'Редактирование гео-метки' : 'Добавление новой гео-метки'); 
    $(markerInfo).find('.markInfo_EditMarkerData').find('.markNewTitleVal').attr('value', mark.title); 
    $(markerInfo).find('.markInfo_EditMarkerData').find('.markNewDescrVal').html(mark.shortDesc);
    $(markerInfo).find('.markNewTypeValue').attr('value', getMarkTypemat(mark));
    $(markerInfo).find('.markInfo_EditMarkerData').find('.markNewTypeSelectBlock').html((mark.t) ? genSelectMarkerType(getMarkTypemat(mark)) : genSelectMarkerType(0,true));

    $(markerInfo).find('.markInfo_EditMarkerData.hidden').removeClass('hidden');
    $(markerInfo).find('.markInfo_MarkerData:not([class*="hidden"])').addClass('hidden'); 
  }else{
    var mark_typemat = window.JS_ENV.data.markerTypes[getMarkTypemat(mark)].n;
    $(markerInfo).find('.markInfo_Header').html(mark_typemat);
    $(markerInfo).find('.markInfo_MarkerData > .mark_title').html((mark.unsaved) ? mark.unsaved_title     : mark.title);
    $(markerInfo).find('.markInfo_MarkerData > .mark_descr').html((mark.unsaved) ? mark.unsaved_shortDesc :mark.shortDesc);
    $(markerInfo).find('.markInfo_MarkerData.hidden').removeClass('hidden');
    $(markerInfo).find('.markInfo_EditMarkerData').not('.hidden').addClass('hidden'); 
    if (mark.e > 0){
      $(markerInfo).find('.mark_edit_link.hidden').removeClass('hidden');
    }else{
      $(markerInfo).find('.mark_edit_link').not('.hidden').addClass('hidden'); 
    }
    
    
    if ((mark.mark_elem) && (mark.mark_elem.draggable) && (markId > 0)){
      if (mark.mark_elem){
        mark.mark_elem.setMap(null);
        mark.mark_elem = undefined; 
      } 
      drawMark(markId, mark, CACHED_MARKERS.getZoom2());
    } 
  }


  
  if (infowindow) infowindow.close();
  infowindow = new google.maps.InfoWindow({ content: markerInfo.innerHTML });
  infowindow.open(map, mark.mark_elem);
  if ((!(mark.moreDataLoaded))&& (!(markId == 0))) {
    mark.moreDataLoaded = true;
    $.post('', {'ajax': true,'type':'getMoreMarksData','data' : JSON.stringify({markIds:[markId]}).replace(/\\"/g, "&quot;")}, function(res, textStatus) {
      if (res.result == 1){ 
        mark.shortDesc = res.markFullInfo[markId].description;
        $infoWindow= $('.gm-style-iw input[name="mark_id"][value="'+markId+'"]').closest('.gm-style-iw');
        if (mark.inEditMode) {
          $infoWindow.find('.markInfo_EditMarkerData').find('.markNewDescrVal').html(mark.shortDesc);
        }else{
          $infoWindow.find('.markInfo_MarkerData > .mark_descr').html((mark.unsaved) ? mark.unsaved_shortDesc :mark.shortDesc);
        }
      }
    }, "json");
  }
  // console.log(infowindow);

}

function showMarkerInfoWindowById(id, center_to_mark){
  $(markerInfo).attr('mark_id',id);
  $(markerInfo).find('input[name="mark_id"]').attr('value',id);
  var mark = getEditMark();
  if (center_to_mark) {
    var center_map =  {lat: mark.lat+0.0005, lng: mark.lng}; 
    map.setCenter(center_map);
  }    
  console.log('showMarkerInfoWindowById id='+id);
  console.log(mark);
  showMarkerInfoWindow();
}





function drawMark(markerId, mark, zoom){
  if (!(mark)) console.error('Ошибка 100501: Тут 100% должна быть метка, а её нету!');

  if (zoom < 3) {
    mark.count = 0;
    for (var f_val in mark.t) {
      f_val = (+f_val);              
      if (((CACHED_MARKERS.filters.length > 0) && (CACHED_MARKERS.filters.indexOf(f_val) >= 0)) || (CACHED_MARKERS.filters.length == 0)){
        mark.count = mark.count + mark.t[f_val]['c'];          
      }
    }           
  }

  var p = {
      position: {lat: parseFloat(mark.lat), lng: parseFloat(mark.lng)},
      labelContent: '',
      optimized: false,
      map: map,
      width: 7,
      // draggable: true,
      labelClass: 'markerLabels',
      icon: '/wthemes/velo/theme/velo/maps/images/transparent.png',
      zindex: 1    
  };    
  
  var currZoom = CACHED_MARKERS.pos.currentZoom;
  switch (zoom){
    case 1:                          
              if(mark.count < 2) {
                  p.width = 7;
                  p.zindex = 1;
              } else if(mark.count < 5) {
                  p.width = 14;
                  p.zindex = 2;
              } else if(mark.count < 100) {
                  p.width = 24;
                  p.zindex = 3;
              } else if(mark.count < 1000) {
                  p.width = 36;
                  p.zindex = 4;
              } else if(mark.count < 10000) {
                  p.width = 42;
                  p.zindex = 5;
              } else if(mark.count < 100000) {
                  p.width = 48;
                  p.zindex = 6;
              } else if(mark.count < 1000000) {
                  p.width = 60;
                  p.zindex = 7;
              } else if(mark.count >= 1000000) {
                  p.width = 74;
                  p.zindex = 8;
              }
            break;
    case 2:         
              p.territory  = mark.territory;
              p.population = mark.population;
              if(mark.count < 2) {
                  p.width = 7;
                  p.zindex = 1;
              } else if(mark.count < 5) {
                  p.width = 14;
                  p.zindex = 2;
              } else if(mark.count < 100) {
                  p.width = 24;
                  p.zindex = 3;
              } else if(mark.count < 1000) {
                  p.width = 36;
                  p.zindex = 4;
              } else if(mark.count < 10000) {
                  p.width = 42;
                  p.zindex = 5;
              } else if(mark.count < 100000) {
                  p.width = 48;
                  p.zindex = 6;
              } else if(mark.count < 1000000) {
                  p.width = 60;
                  p.zindex = 7;
              } else if(mark.count >= 1000000) {
                  p.width = 74;
                  p.zindex = 8;
              }
            break;
    default:
              
              p.title       = mark.title;
              // p.shortDesc   = mark.shortDesc;
              p.markId      = markerId;              
              if (currZoom >= MIN_ZOOM_FOR_ICONS_DRAW){
                var mt = getMarkTypemat(mark);
                var parent_mt = (window.JS_ENV.data.markerTypes[mt]) ? window.JS_ENV.data.markerTypes[mt].p : 0;
                var icon_index= (icons2[mt]) ? icons2[mt] : (((parent_mt > 0) && (icons2[parent_mt]))? icons2[parent_mt] : 'no_marker.png');
                p.labelZIndex = p.zindex;
                p.icon        = MARKERS_IMAGE_DIR+'/'+icon_index+'';
              }else{
                p.width = 74;
                p.zindex = p.labelZIndex+100;
              }
              // p.icon        = '/wthemes/velo/dekor/markers/' + icons[mark.icon] + '.png';
  }
  var markClass = 'm'+((p.zindex > 1) ? p.zindex - 2 : '00');
  if (zoom >= 3) {
    markClass = (currZoom >= MIN_ZOOM_FOR_ICONS_DRAW) ? 'm2' : 'm00';
  }
  p.labelContent = (currZoom >= MIN_ZOOM_FOR_ICONS_DRAW) ? '' : '<div class="markLabelWrap ' + markClass + '"><div class="inner">' + ((zoom >= 3) ? '' : mark.count ) + '</div></div>';    
  // if (currZoom < MIN_ZOOM_FOR_ICONS_DRAW) p.labelContent = '<div class="markLabelWrap ' + markClass + '"</div>';
  p.labelClass = (currZoom >= MIN_ZOOM_FOR_ICONS_DRAW) ? '' : p.labelClass+' '+markClass +' transprnt'+zoom;

  CACHED_MARKERS.c[zoom][markerId].mark_elem = new MarkerWithLabel(p);

  if (zoom < 3){
    google.maps.event.addListener(CACHED_MARKERS.c[zoom][markerId].mark_elem, 'click', function(){
      map.setZoom((zoom == 1) ? 8: MIN_ZOOM_FOR_ICONS_DRAW);
      // console.log(mark);
      map.setCenter({lat: parseFloat(this.position.lat()), lng: parseFloat(this.position.lng())});  
      // mapZoomChange((zoom == 1) ? 8: 15, CACHED_MARKERS.c[zoom][markerId].lat, CACHED_MARKERS.c[zoom][markerId].lng);

      console.log(this.position.lat());
      console.log(this.position.lng());
    });
  }else{
    google.maps.event.addListener(CACHED_MARKERS.c[zoom][markerId].mark_elem, 'click', function(){
      showMarkerInfoWindowById(markerId, false);
    });
  }
}

// var n = 100000;
    // var array = new Array(n);
    // var uint32Array = new Uint32Array(n);
// Тут будут храниться те метки, которые уже загружены. Также будут храниться уже загруженые области для каждого слоя.
var CACHED_MARKERS={
  m : [{},{},{},{}], // markers
  c : [{},{},{},{}], // marks cache
  f : {}, // FLAGS всё что будет тут - будет добавляться автоматически. Переменные, которые тут будут храниться не будут инициализированы здесь.
  pos : { // позиция экрана юзера на карте.
    latLng           : [0,0,0,0], // rectangle экрана юзверя.
    currentCenter    : new google.maps.LatLng(49.44670029695473, 32.0635986328125), // Центр по умолчанию.
    currentZoom      : 6, // zoom по умолчанию.
    currentZoomValue : 1
  },
  mapType : 0,
  // oldfilterSettings: [], // эту дрочь надо убирать
  // newfilterSettings:[],  // и эту дрочь надо тоже убирать!.
  filters : [],
  printedMarksIds : [[],[],[],[]],
  XHR_request : new XMLHttpRequest(),  
  redrawInBackgroundData : {
    needRedrawOtherZoom: true,// при смене зума ставится флаг need=1, таймер видит этот флаг начинает обработку и ставит need=0 и running=1
    runningRedrawOtherZoom: false,
    fastLatLng : [0,0,0,0],
    setFastLatLng : function (latLng){
      var h_add = (latLng[0] - latLng[2]) / 4;// по 25% добавим сверху и снизу.
      var w_add = (latLng[1] - latLng[3]) / 4;// по 25% добавим слева  и справа.
      this.fastLatLng = [latLng[0]+h_add,latLng[1]+w_add, latLng[2]-h_add, latLng[3]-w_add];
    },
    zoom_value : 1,
    cur_index  : 0,
    maxIndexCountPerInterrupt : 100, // Если ненадо ни затирать, ни добавлять метку на карту, то будет обрабатываться такое количество "холостых" точек за одно прерывание таймера
    backgroundRedrawTimer : undefined,
    runningRedraw : false, // После того как пройдёт круг всех меток и экран не будет шевелиться - этот флаг ставится в 0 и ждёт флага needRedraw
    needRedraw : false,
    working_flag : false,
    changeZoomValue : function(newValue){
      if ((+newValue) != this.zoom_value){  
        this.cur_index = 0;      
        this.zoom_value = newValue;  
        this.runFastRedraw();              
      }
    },
    runFastRedraw : function (count){
      // console.log('runFastRedraw');
      count = count || 100000; // По умолчанию для быстрой прорисовки будем применять 1000;
      this.runningRedraw = true;
      this.needRedraw = false;
      this.cur_index = 0; // это повторное присвоение нужно ( а то вдруг во время установки флагов заточится срабатывание таймера перерисовки? )
      this.maxIndexCountPerInterrupt = count;
    }
  }, 
  setMapType : function(new_map_type){
    new_map_type = (mapTypes[new_map_type] > 0) ? mapTypes[new_map_type] : 0;
    if (new_map_type != this.mapType){
      this.mapType = new_map_type;
      this.clearCache();
      if (this.f.map_initialized) this.runLoadMarkers();
    }
    console.log('setMapType='+this.mapType);
  },
  clearCache : function(){
    this.clearOtherZooms(0);
    this.m               = [{},{},{},{}];
    this.c               = [{},{},{},{}];
    this.printedMarksIds = [[],[],[],[]];
  },
  addResponse: function(response){

    var zoom = (response.info.zoom > 0) ? (+response.info.zoom) : 1;
    if (!(this.m[zoom])){ this.m[zoom] ={}; }
    if (!(this.c[zoom])){ this.c[zoom] ={}; }
    // console.log(response);
    for (var i = 0; i < response.request_data.filter.length; i++) {  
      var filter_val = (+response.request_data.filter[i]);  
      if (!(this.m[zoom][filter_val])){
        this.m[zoom][filter_val] = {
          'areas_loaded' : [],
          'marksIds'     : []
        };
      }   
      for (var markerId in response.data) {
        markerId = (+markerId);
        if (this.m[zoom][filter_val]['marksIds'].indexOf(markerId) >= 0) continue;
        this.m[zoom][filter_val]['marksIds'].push(markerId);
        // console.log('push into zoom='+zoom+' filter_val='+filter_val+' marksIds='+markerId);
      }      
      this.m[zoom][filter_val]['areas_loaded'].push(response.info.latLng);
      // console.log(this.m);
    }
    var add_id = false;
    for (var markerId in response.data) {
      markerId = (+markerId);
      var mark = this.c[zoom][markerId]; 
      if (mark){
        if (response.data[markerId].save_result) markSaveChangesDone(markerId,response.data[markerId].save_result);        
        // if (mark.unsaved) mark.unsaved = false;        
        for (var t in response.data[markerId]['t']) {
          t = (+t);     
          if (t > 0){
            if (!(mark['t'][t])){
              mark['t'][t] = response.data[markerId]['t'][t];
              var indx = this.printedMarksIds[zoom].indexOf(markerId);
              if (indx >= 0){ 
                this.printedMarksIds[zoom].splice(indx, 1);
                if (mark.mark_elem){            
                  mark.mark_elem.setMap(null);
                  mark.mark_elem = undefined;                
                }
                add_id = true;
              } 
              // console.log(this.c[zoom][markerId]);                   
            } 
          }               
        }
        // console.log(this.c[zoom][markerId]);
      }else{
        add_id = true;
        this.c[zoom][markerId] = {
          lat: parseFloat(response.data[markerId].lat),
          lng: parseFloat(response.data[markerId].lng),  
          // latLng      : {lat: parseFloat(response.data[markerId].lat), lng: parseFloat(response.data[markerId].lng)},
          title       : response.data[markerId].title,
          t           : response.data[markerId].t,          
          icon        : response.data[markerId].typemat,         
        };
        if (response.data[markerId].e > 0) this.c[zoom][markerId].e = 1;
        // response.data[markerId];
      } 
      
    }
    if (add_id) {      
      // console.log('added for zoom='+zoom+' ['+add_id+']');  
      // console.log(this.m);
      // console.log('add_id');
      this.redrawInBackgroundData.runFastRedraw();  
      // this.redrawMarkers();
    }
    // console.log(this.m);  
  },
  clean: function(){
    console.log('CACHED_MARKERS clean!!!');
    // this.m = {};
  },     
  saveToCookies: function(){    
    // console.log('saveToCookies');
    var cook = {
      p       : this.pos,
      f       : this.filters,
      version : 1      
    };
    setcookie(window.location.hostname.replace(/(\.[\s\S]*)$/, '')+'_'+'user_display_pos', JSON.stringify(cook), 2592000, '/ru/velo_maps');    
  },
  loadFromCookies: function(){
    var cook = getcookie(window.location.hostname.replace(/(\.[\s\S]*)$/, '')+'_'+'user_display_pos');
    if ((cook)&&(cook.length > 0) && (cook = JSON.parse(cook)) && (cook.version == 1)) {
      this.pos = cook.p;    
      this.filters = cook.f || [907, 918, 920, 921, 922, 924, 923, 925, 908,911,940,941,917,942,943,909,913,914,944,945,946,947,927,953,948,949,950,951,952,954,928,955,956,957,958,959,929,960,961,930,977,978,931,938,939,979,980,981,982,984,932,988,989,990,991,992,933,983,986,987,934,993,994,1008,964,965,966,967,963,968,969,970,971,972,973,974,975,976,935,995,996,1007,936,962,997,937,998,999,1000,1001,1002,1003,1004,1005,1006];    
    }else{
      this.filters = [907, 918, 920, 921, 922, 924, 923, 925, 908,911,940,941,917,942,943,909,913,914,944,945,946,947,927,953,948,949,950,951,952,954,928,955,956,957,958,959,929,960,961,930,977,978,931,938,939,979,980,981,982,984,932,988,989,990,991,992,933,983,986,987,934,993,994,1008,964,965,966,967,963,968,969,970,971,972,973,974,975,976,935,995,996,1007,936,962,997,937,998,999,1000,1001,1002,1003,1004,1005,1006];
      this.saveToCookies();
    }  
  },
  init: function(){
    var get_map_type = window.JS_ENV.get_params.map_type;
    if (get_map_type) this.setMapType(get_map_type); 
    this.loadFromCookies();
    this.pos.currentZoomValue = this.getZoom2();
    setInterval( function(){ CACHED_MARKERS.saveToCookies(); }, 10000); // будем писать в куку только каждые 10 секунд а не постоянно! 
    this.redrawInBackgroundData.backgroundRedrawTimer = setInterval( function(){ CACHED_MARKERS.redrawMarkersInBackground(); }, 1); //фоновый таймер перерисовки меток
    setInterval( function(){ 
      if (CACHED_MARKERS.f.need_redraw_fast){
        CACHED_MARKERS.redrawMarkers_fast(); 
      }
    }, 3000); // будем перерисовывать только каждые 3 секунды а не постоянно! вообще было бы круто в фоне это сделать.... Но пока будет так...
    this.XHR_request.onreadystatechange = function() {
        if(CACHED_MARKERS.XHR_request.readyState == 4) {
          if(CACHED_MARKERS.XHR_request.status == 200) {            
              var response = CACHED_MARKERS.XHR_request.responseText;
              try{
                  response = JSON.parse(response);
                  CACHED_MARKERS.addResponse(response);
              } catch (e) {
                  console.error('2 PARSE json error!!');
                  console.log(e);
              //     return false;
              }  
          }
          CACHED_MARKERS.f.XHR_buzy = false;
          if (CACHED_MARKERS.f.need_reload){
            CACHED_MARKERS.f.need_reload = false;
            CACHED_MARKERS.runLoadMarkers();
          } 
        }
    };
  },
  getZoom2: function(zoom){
    zoom = zoom || this.pos.currentZoom;
    zoom = (zoom <= 6) ? 1 : ((zoom <= 11) ? 2 : ((zoom <= 13) ? 3 : 3));
    return zoom;
  },
  redrawMarkers: function(){
    if (this.redrawInBackgroundData.runningRedraw){
      this.redrawInBackgroundData.needRedraw = true;       
    }else{
      this.redrawInBackgroundData.runningRedraw = true;
    }
  },
  redrawMarkersInBackground: function(){    
    var t = this.redrawInBackgroundData;
    if (t.working_flag){
      // console.log('working_flag! ');
      return;
    } 
    t.working_flag = true;    
    var fast_redraw = (t.maxIndexCountPerInterrupt > 1000);
    // console.log('fast_redraw='+fast_redraw);
    
    if (t.runningRedraw){
      var filters_tmp = (this.filters.length >= 1) ? this.filters : [1];
      var k = 0;
      var start_index = t.cur_index;
      var end_index   = t.cur_index + t.maxIndexCountPerInterrupt;
      var fullRound = true;
      var print_points_count = (fast_redraw) ? 100 : 1;
      for (var markerId in this.c[t.zoom_value]) {
        k++;
        if (k > start_index){        
          markerId = (+markerId);
          var mark = this.c[t.zoom_value][markerId];
          if (!mark) continue;
          if (mark.inEditMode) continue;
          var mark_placed_in_filters = false;// проверим принадлежит ли метка выбраным фильтрам 
          if (this.filters.length >= 1){
            for (var f_val in mark.t) {
              f_val = (+f_val);              
              if (this.filters.indexOf(f_val) >= 0) {
                mark_placed_in_filters = true;                
              }          
            }
          }else{
            mark_placed_in_filters = true;
          }
          var markPlacedInScreenRectangle = false;
          
          if (mark_placed_in_filters){            
            markPlacedInScreenRectangle = ((mark.lat < t.fastLatLng[0]) &&(mark.lng < t.fastLatLng[1]) &&(mark.lat > t.fastLatLng[2]) &&(mark.lng > t.fastLatLng[3]));
          }
          // console.log('markerId='+markerId+' mark_placed_in_filters='+mark_placed_in_filters+' markPlacedInScreenRectangle='+markPlacedInScreenRectangle);  
          var indx = this.printedMarksIds[t.zoom_value].indexOf(markerId);
          if (mark_placed_in_filters && markPlacedInScreenRectangle){  
            if (indx < 0){ 
              this.printedMarksIds[t.zoom_value].push(markerId);         
              drawMark(markerId, mark, t.zoom_value);  
              print_points_count--;
              if (print_points_count <=0 ) {fullRound = false; t.cur_index++;  break;}                       
              // if (!fast_redraw) {fullRound = false; t.cur_index++;  break;  }
            }
          }else{
            if (indx >= 0){ 
              this.printedMarksIds[t.zoom_value].splice(indx, 1);
              if (mark.mark_elem){            
                mark.mark_elem.setMap(null);
                mark.mark_elem = undefined;
                if (!fast_redraw) {fullRound = false; t.cur_index++;  break;  }
              }
            }
          }
          t.cur_index++; 
        }        
        if (k > end_index){
          fullRound = false;
          break;
        } 
      }
      if (fullRound){
        t.cur_index = 0;
        if (t.needRedraw){  
          t.needRedraw =false; // Если надо перерисовывать - ставим флаг что мы пошли перерисовывать, и не останавливаем рисовалку.
        }else{
          // console.log('STOP redrawTimer!');
          this.refreshObjectsInfoBlock();
          t.runningRedraw = false; // а если ненадо перерисовывать - значит всё, необходимости в этом таймере нету... можно разрушивать таймер в этом месте, но мне лень, поэтому флаг :)...
        }
      }
                
    }
    if (fast_redraw){
      t.maxIndexCountPerInterrupt   = 1000;  
    }
    t.working_flag = false;
  },
  clearZoom: function(zoom){
    for (var j = this.printedMarksIds[zoom].length; j > 0; j--){                
      var markerId = this.printedMarksIds[zoom].pop();
      var mark = this.c[zoom][markerId];
      if ((mark) && (mark.mark_elem)){            
        mark.mark_elem.setMap(null);
        mark.mark_elem = undefined;          
      }
    }
  },
  clearOtherZooms : function(zoom){
    for (var i = 0; i < this.printedMarksIds.length; i++) { 
      if (i == zoom) continue;
      this.clearZoom(i);
    }
  },
  redrawMarkers_fast: function(){
    this.f.need_redraw_fast = false;
    // console.log('redrawMarkers_fast');
    var t = this.redrawInBackgroundData;              
        t.zoom_value = this.getZoom2();
        t.runFastRedraw();        
    this.clearOtherZooms(t.zoom_value);
  },
  runLoadMarkersIfNeed: function(){
    var zoom = this.getZoom2();
    if (!(this.m[zoom])){ this.m[zoom] ={}; }
    if (!(this.c[zoom])){ this.c[zoom] ={}; }
    var filters_tmp = this.filters; // [1] - это типа всё включено.
    var lat = this.pos.latLng;
    var need_load_data = true;
    var need_load_filters = [];
    if ((this.m[zoom][1]) && (this.m[zoom][1]['areas_loaded']) && (this.m[zoom][1]['areas_loaded'].length > 0)){
      for (var i =0; i < this.m[zoom][1]['areas_loaded'].length; i++) { 
        var l = this.m[zoom][1]['areas_loaded'][i];
        if ((l[0]>lat[0]) && (l[1]>lat[1]) && (l[2]<lat[2]) && (l[3]<lat[3])){
          need_load_data = false;
          break;
        }
      }
    }
    // Если нету в выборке без фильтров - проверяем может есть эта зона в каждом выбраном фильтре
    if (need_load_data) {  
      if (filters_tmp.length > 0){
        for (var f = 0; f < filters_tmp.length; f++) {  
          var filter_val = filters_tmp[f];
          if (!(this.m[zoom][filter_val])){   
            this.m[zoom][filter_val] = {
              'areas_loaded' : [],
              'marksIds'     : []
            };
          }
          var need_load_data_for_this_filter = true;
          for (var i =0; i < this.m[zoom][filter_val]['areas_loaded'].length; i++) { 
            var l = this.m[zoom][filter_val]['areas_loaded'][i];
            if ((l[0]>lat[0]) && (l[1]>lat[1]) && (l[2]<lat[2]) && (l[3]<lat[3])){
              need_load_data_for_this_filter = false;
              break;
            }
          }
          if (need_load_data_for_this_filter){ 
            need_load_filters.push(filter_val);          
          }
        }
      }else{
        need_load_filters.push(1); 
      }
    }

    if ((need_load_filters.length > 0) || (this.f.NEED_SAVE_MARK)){
      // console.log('Для zoom='+zoom+' ['+lat.join(',')+'] надо грузить!');
      this.runLoadMarkers2(need_load_filters);
    }

  },
  runLoadMarkers : function(){
    this.runLoadMarkersIfNeed();
    this.redrawMarkers();
  },
  ObjectsInfoBlock : {
    infoBlock  : document.getElementById('infoBlock_wrapper'),
    printedData:[],
    old_zoom : 0,    
    redrawSortedList : function(){
      // console.log('redrawSortedList');
      // console.log(this.printedData);
      var last_id = 0;
      for(var i = this.printedData.length-1; i>=0; i--) {
        var src = this.printedData[i];
        $this_element = $(this.infoBlock).children('div[src="'+src+'"]');
        if ($this_element.length == 0){
          var m = CACHED_MARKERS.c[this.old_zoom][src];
          var typemat = (this.old_zoom >=3) ? getMarkTypemat(m) : 0;   
          var object_type = (this.old_zoom >=3) ?  window.JS_ENV.data.markerTypes[typemat].n : ((this.old_zoom == 1) ? "Область" :"Населённый пункт");
          var style= "";
          if (typemat > 0){
            var img_url = MARKERS_IMAGE_DIR+'/'+icons2[typemat];
            style = ' style="background-image: url(\''+img_url+'\');"';
          }
          var new_item = '<div class="object-wrap" src="'+src+'" '+style+'><span class="object-name">'+m.title+'</span><span class="object-type">'+object_type+'</span><span class="object-addr hidden"></span><div class="object-link"><span class="object-link--text" onClick="goToMark('+src+','+m.lat+','+m.lng+');">Перейти на позицию</span></div></div>';        
          // $last_element = $(this.infoBlock).append($(new_item));
          if (last_id > 0) {
            $(new_item).insertBefore($(this.infoBlock).children('div[src="'+last_id+'"]'));            
          }else{
            $(this.infoBlock).append($(new_item));
            // $last_element = $(this.infoBlock).children('div[src="'+src+'"]');
          }               
        }   
        last_id = src;     
      }
    },

    del : function(src){
      var indx = this.printedData.indexOf(src); 
      if (indx >=0){
        this.printedData.splice(indx, 1);
        $(this.infoBlock).children('div[src="'+src+'"]').remove();
      } 
      // console.log('del from info src='+src);
    },
  },
  refreshObjectsInfoBlock : function(){
    var t   = this;
    var r   = this.redrawInBackgroundData;
    if (t.ObjectsInfoBlock.old_zoom != r.zoom_value){
      t.ObjectsInfoBlock.old_zoom = r.zoom_value;
      printedData = [];
    }
    $(t.ObjectsInfoBlock.infoBlock).children().each(function(){
      var id = (+$(this).attr('src'));
      if (!(id > 0)) id = 0;
      if (id>0){
        var indx = t.printedMarksIds[r.zoom_value].indexOf(id);     
        if (indx < 0){
          t.ObjectsInfoBlock.del(id);
        }   
      }
    });
    for(var i = t.printedMarksIds[r.zoom_value].length-1; i>=0; i--) {
      var mark_id = t.printedMarksIds[r.zoom_value][i];
      var mark = t.c[r.zoom_value][mark_id];
      var latLng = t.pos.latLng;
      //Метки рисуются немного больше чем экран, поэтому будем проверять входим ли мы в границы экрана
      var markPlacedInScreenRectangle = ((mark.lat < latLng[0]) &&(mark.lng < latLng[1]) &&(mark.lat > latLng[2]) &&(mark.lng > latLng[3]));
      var indx = t.ObjectsInfoBlock.printedData.indexOf(mark_id);  
      if ((indx < 0) && (markPlacedInScreenRectangle)){ 
                    
        t.ObjectsInfoBlock.printedData.push(mark_id);         
        // t.ObjectsInfoBlock.add(t.c[r.zoom_value][mark_id], mark_id);          
      }else{
        if (!(markPlacedInScreenRectangle)){
          if (indx >=0)t.ObjectsInfoBlock.del(mark_id);
        }
      }
      // console.log(latLng);
      // console.log(mark.lat+' '+mark.lng+" mark_id="+mark_id+" zoom="+r.zoom_value+" placed="+markPlacedInScreenRectangle+" 0="+ (mark.lat < latLng[0])+" 1="+(mark.lng < latLng[1])+" 2="+(mark.lat > latLng[2])+"3="+(mark.lng > latLng[3]));
      // console.log(t.printedMarksIds[r.zoom_value]);
    }
    t.ObjectsInfoBlock.printedData.sort(this.marksSortFunction);
    t.ObjectsInfoBlock.redrawSortedList();
    // this.printedMarksIds[t.zoom_value].splice(indx, 1);
   

    
  },
  marksSortFunction : function(a, b){
    var r   = CACHED_MARKERS.redrawInBackgroundData;
    return compareStrings(CACHED_MARKERS.c[r.zoom_value][a].title.toUpperCase(),CACHED_MARKERS.c[r.zoom_value][b].title.toUpperCase());
  },
  

  runLoadMarkers2 : function(need_load_filters){
    if (this.f.XHR_buzy){   
      this.f.need_reload = true;
      // this.f.XHR_buzy = false;
      return;
    }
    this.f.XHR_buzy = true;
    try { this.XHR_request.abort(); } catch (e) {console.error(e);}
    var h_add = (this.pos.latLng[0] - this.pos.latLng[2]) / 2;// по 50% добавим сверху и снизу.
    var w_add = (this.pos.latLng[1] - this.pos.latLng[3]) / 2;// по 50% добавим слева  и справа.
    var request_data = {
      latLng   : [this.pos.latLng[0]+h_add, this.pos.latLng[1]+w_add, this.pos.latLng[2]-h_add, this.pos.latLng[3]-w_add],
      fullZoom : this.pos.currentZoom,
      filter   : need_load_filters      
    };

    if (this.f.NEED_SAVE_MARK) {
      request_data.save = this.f.NEED_SAVE_MARK;      
      this.f.NEED_SAVE_MARK = undefined;
      console.log(request_data);
    }


    // console.log('l zoom=['+request_data.fullZoom+'] added=['+request_data.latLng.join(',')+']');

    this.XHR_request.open('POST', '/ru/velo_maps', true);
    this.XHR_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    this.XHR_request.send('type=getMarkers&request_data=' + JSON.stringify(request_data) + '&latLng=' + JSON.stringify(request_data.latLng) + '&zoom=' + this.pos.currentZoom + '&ajax=true');    
  }

};


function mapInitialize() {
    var mapCanvas = document.getElementById('velogen-map');

    var header_height = document.getElementsByTagName('header');
    header_height = header_height[0].offsetHeight;

    var copyright_height = document.getElementsByClassName('main-copy-wrapper');
    copyright_height = copyright_height[0].offsetHeight ;

    var map_height = screen.height - header_height - copyright_height - 91;

    mapCanvas.style.height = map_height + 'px';
    mapCanvas.style.width  = screen.width + 'px';


    CACHED_MARKERS.loadFromCookies();
    var mapOptions = {
        center: CACHED_MARKERS.pos.currentCenter,
        mapTypeControl: true,
        minZoom: 6,
        zoom: CACHED_MARKERS.pos.currentZoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(mapCanvas, mapOptions);
    CACHED_MARKERS.init();
    // map.setZoom(CACHED_MARKERS.pos.currentZoom);
    // map.setCenter({lat: CACHED_MARKERS.pos.currentCenter.lat, lng: CACHED_MARKERS.pos.currentCenter.lng});       
    google.maps.event.addListener( map, 'idle',  function() {
      if (!(CACHED_MARKERS.f.map_initialized)){
        CACHED_MARKERS.f.map_initialized = true;        
        console.log('map init done! idle run');
        // google.maps.event.addListener(map, 'dragend', function() { 
        //   console.log('map dragged'); 
        //   map_drag_end();
        // });
        // console.log(window.JS_ENV);  
        map.addListener('zoom_changed',   mapZoomChange);
        map.addListener('center_changed', mapCenterChange);
        // applyFilter(true);
        mapCenterChange(map); 
        CACHED_MARKERS.f.need_redraw_fast = true;               
      }      
    });
}

function mapCenterChange(map_in) {    
    var cur_map = map_in || this;
    CACHED_MARKERS.pos.currentCenter = cur_map.getCenter();    

    var lat0 = cur_map.getBounds().getNorthEast().lat();
    var lng0 = cur_map.getBounds().getNorthEast().lng();
    var lat1 = cur_map.getBounds().getSouthWest().lat();
    var lng1 = cur_map.getBounds().getSouthWest().lng();

    CACHED_MARKERS.pos.latLng = [lat0, lng0, lat1, lng1];
    // console.log(CACHED_MARKERS.pos.latLng);
    CACHED_MARKERS.redrawInBackgroundData.setFastLatLng(CACHED_MARKERS.pos.latLng);
    CACHED_MARKERS.runLoadMarkers();
}

function mapZoomChange() {
    var _currentZoom = (+this.getZoom());
    var oz = CACHED_MARKERS.pos.currentZoom;
    var zoom_changed = (_currentZoom != CACHED_MARKERS.pos.currentZoom);  
    CACHED_MARKERS.pos.currentZoom = _currentZoom;
    CACHED_MARKERS.pos.currentZoomValue = CACHED_MARKERS.getZoom2(_currentZoom);    

    if ((_currentZoom >= MIN_ZOOM_FOR_ICONS_DRAW) && (oz < MIN_ZOOM_FOR_ICONS_DRAW)){
      CACHED_MARKERS.clearZoom(CACHED_MARKERS.getZoom2(oz));
    }

    if ((_currentZoom < MIN_ZOOM_FOR_ICONS_DRAW) && (oz >= MIN_ZOOM_FOR_ICONS_DRAW)){
      CACHED_MARKERS.clearZoom(CACHED_MARKERS.getZoom2(oz));
    }
    // if(_currentZoom) map.setZoom(CACHED_MARKERS.pos.currentZoom);
    var lat0 = this.getBounds().getNorthEast().lat();
    var lng0 = this.getBounds().getNorthEast().lng();
    var lat1 = this.getBounds().getSouthWest().lat();
    var lng1 = this.getBounds().getSouthWest().lng();
    CACHED_MARKERS.pos.latLng = [lat0, lng0, lat1, lng1];
    CACHED_MARKERS.redrawInBackgroundData.setFastLatLng(CACHED_MARKERS.pos.latLng);
    CACHED_MARKERS.redrawInBackgroundData.changeZoomValue(CACHED_MARKERS.pos.currentZoomValue);
    // console.log('mapZoomChange zoom_changed='+zoom_changed+' _currentZoom='+_currentZoom);
    if (zoom_changed) {
      // console.log('zoom_changed');
      CACHED_MARKERS.f.need_redraw_fast = true;
      CACHED_MARKERS.runLoadMarkers();    
    }
}

function decodeHtmlEntity (str) {
    if( ! str || ! str.length)
        return;

    return str.replace(/&#?(\w+);/g, function(match, dec) {
        if(isNaN(dec)) {
            chars = {quot: 34, amp: 38, lt: 60, gt: 62, nbsp: 160, copy: 169, reg: 174, deg: 176, frasl: 47, trade: 8482, euro: 8364, Agrave: 192, Aacute: 193, Acirc: 194, Atilde: 195, Auml: 196, Aring: 197, AElig: 198, Ccedil: 199, Egrave: 200, Eacute: 201, Ecirc: 202, Euml: 203, Igrave: 204, Iacute: 205, Icirc: 206, Iuml: 207, ETH: 208, Ntilde: 209, Ograve: 210, Oacute: 211, Ocirc: 212, Otilde: 213, Ouml: 214, times: 215, Oslash: 216, Ugrave: 217, Uacute: 218, Ucirc: 219, Uuml: 220, Yacute: 221, THORN: 222, szlig: 223, agrave: 224, aacute: 225, acirc: 226, atilde: 227, auml: 228, aring: 229, aelig: 230, ccedil: 231, egrave: 232, eacute: 233, ecirc: 234, euml: 235, igrave: 236, iacute: 237, icirc: 238, iuml: 239, eth: 240, ntilde: 241, ograve: 242, oacute: 243, ocirc: 244, otilde: 245, ouml: 246, divide: 247, oslash: 248, ugrave: 249, uacute: 250, ucirc: 251, uuml: 252, yacute: 253, thorn: 254, yuml: 255, lsquo: 8216, rsquo: 8217, sbquo: 8218, ldquo: 8220, rdquo: 8221, bdquo: 8222, dagger: 8224, Dagger: 8225, permil: 8240, lsaquo: 8249, rsaquo: 8250, spades: 9824, clubs: 9827, hearts: 9829, diams: 9830, oline: 8254, larr: 8592, uarr: 8593, rarr: 8594, darr: 8595, hellip: 133, ndash: 150, mdash: 151, iexcl: 161, cent: 162, pound: 163, curren: 164, yen: 165, brvbar: 166, brkbar: 166, sect: 167, uml: 168, die: 168, ordf: 170, laquo: 171, not: 172, shy: 173, macr: 175, hibar: 175, plusmn: 177, sup2: 178, sup3: 179, acute: 180, micro: 181, para: 182, middot: 183, cedil: 184, sup1: 185, ordm: 186, raquo: 187, frac14: 188, frac12: 189, frac34: 190, iquest: 191, Alpha: 913, alpha: 945, Beta: 914, beta: 946, Gamma: 915, gamma: 947, Delta: 916, delta: 948, Epsilon: 917, epsilon: 949, Zeta: 918, zeta: 950, Eta: 919, eta: 951, Theta: 920, theta: 952, Iota: 921, iota: 953, Kappa: 922, kappa: 954, Lambda: 923, lambda: 955, Mu: 924, mu: 956, Nu: 925, nu: 957, Xi: 926, xi: 958, Omicron: 927, omicron: 959, Pi: 928, pi: 960, Rho: 929, rho: 961, Sigma: 931, sigma: 963, Tau: 932, tau: 964, Upsilon: 933, upsilon: 965, Phi: 934, phi: 966, Chi: 935, chi: 967, Psi: 936, psi: 968, Omega: 937, omega: 969}
            if (chars[dec] !== undefined){
                dec = chars[dec];
            }
        }
        return String.fromCharCode(dec);
    });
};

function goToMark(mark_id, lat, lng){
  map.panTo(new google.maps.LatLng(lat, lng));
  showMarkerInfoWindowById(mark_id, false);
}

$(function(){
    $('.regions-list > li > b').unbind('click').bind('click', function(){
        $(this).parent().toggleClass('opened');//for changing icon  //by alph 12.01.16 12:54
        $(this).parent().find('ul').toggle();
        $(this).parent().find('ul li').unbind('click').bind('click', function(){
            var latltg = $(this).attr('coordinates').split(' ');

            map.panTo(new google.maps.LatLng(latltg[0], latltg[1]));

            // var latLang = {lat: latltg[0], lng: latltg[1]};

            if(marker)
                marker.setMap(null);


            marker = new google.maps.Marker({
                position: {lat: parseFloat(latltg[0]), lng: parseFloat(latltg[1])},
                map: map,
                title: ''
            });
        })
    });
});

google.maps.event.addDomListener(window, 'load', mapInitialize);




document.getElementById('apply-markers-filter').addEventListener('click', applyFilter);

if(typeof  getcookie('filter') != 'undefined')
    var filterSettings = JSON.parse(getcookie('filter'));
else
    var filterSettings = [];

function openSecondLevel() {
    var currentLevel = this.parentNode.getElementsByClassName('child-markers');

    currentLevel = currentLevel[0];

    if((" " + currentLevel.className + " ").replace(/[\n\t]/g, " ").indexOf(" open") > -1 ) {
        currentLevel.setAttribute('class', 'child-markers');
        currentLevel.style.display = 'none';
    } else {
        currentLevel.setAttribute('class', 'child-markers open');
        currentLevel.style.display = 'block';
    }
}

function filterCheckboxChanged(elem) {
  if ($(elem).attr('type') == 'checkbox'){
    var $title_span = $(elem).parent().children('.has-childs__title');
    if ($title_span.length == 0){
      changeFilterSettings(elem);
    }else{
      $(elem).parent().children('ul.child-markers').children('li').children('input[type="checkbox"]').prop('checked', $(elem).prop('checked'));
      if ($(elem).prop('checked')){
        $(elem).parent().children('.has-childs__title').not('.chk_on').addClass('chk_on');
      }else{
        $(elem).parent().children('.has-childs__title.chk_on').removeClass('chk_on');
      }   
      $(elem).parent().children('ul.child-markers').children('li').children('input[type="checkbox"]').each(function(){
        changeFilterSettings(this);
      });
    }     
  }
}

function changeFilterSettings(elem) {
    var new_val = (+elem.getAttribute('ns-id'));
    var indx = CACHED_MARKERS.filters.indexOf(new_val);
    var zoom = CACHED_MARKERS.getZoom2();
    // console.log('new_val='+new_val);
    if(elem.checked){
      if (indx < 0 ){
        CACHED_MARKERS.filters.push(new_val);
        if (zoom < 3) CACHED_MARKERS.clearZoom(zoom);
        CACHED_MARKERS.runLoadMarkers();
        CACHED_MARKERS.redrawInBackgroundData.runFastRedraw(); 
      }
    } else {      
      if (indx >=0 ){
        CACHED_MARKERS.filters.splice(indx, 1);
        if (zoom < 3) CACHED_MARKERS.clearZoom(zoom);
        CACHED_MARKERS.runLoadMarkers();
        CACHED_MARKERS.redrawInBackgroundData.runFastRedraw(); 
      }
    }    
    
    console.log(CACHED_MARKERS.filters);
}

function applyFilter(skip_loadMarkers) {
  CACHED_MARKERS.saveToCookies();
}



var markerFilter = document.getElementById('marker-filter');

var firstLevelFilter = markerFilter.getElementsByClassName('first-level');

for(var i = 0; i < firstLevelFilter.length; i++) {
    var currentLevel = firstLevelFilter[i];
    var currentLabel = currentLevel.getElementsByTagName('span');

    currentLabel = currentLabel[0];


    if(currentLevel.getElementsByClassName('child-markers').length) {
        var currentLevel = currentLabel.parentNode.getElementsByClassName('child-markers');

        currentLevel = currentLevel[0];
        currentLabel.addEventListener('click', openSecondLevel);

        var checkBoxes = currentLevel.getElementsByTagName('input');

        // for(var _i = 0; _i < checkBoxes.length; _i++)
            // checkBoxes[_i].addEventListener('change', changeFilterSettings);
    } else {
        var checkBoxes = currentLevel.getElementsByTagName('input');

        // for(var _i = 0; _i < checkBoxes.length; _i++)
            // checkBoxes[_i].addEventListener('change', changeFilterSettings);
    }
}

function setcookie(name, value, expires, path, domain, secure) {
    expires instanceof Date ? expires = expires.toGMTString() : typeof(expires) == 'number' && (expires = (new Date(+(new Date) + expires * 1e3)).toGMTString());
    var r = [name + "=" + escape(value)], s, i;
    for(i in s = {expires: expires, path: path, domain: domain}){
        s[i] && r.push(i + "=" + s[i]);
    }
    return secure && r.push("secure"), document.cookie = r.join(";"), true;
}

function getcookie(name) {
    var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}


// styling the selects
$(document).ready(function(){
          

  $(document).bind('click', function(e) {
    var $clicked = $(e.target);
    if (! $clicked.parents().hasClass("dropdown")){
      $(".dropdown dd ul").hide();
    }else{
      $(".dropdown").not('[level="'+$clicked.closest('.dropdown').attr('level')+'"]').find('dd ul').hide();
    }
        
  });
  // $.post('', {'ajax': true,'type':'getJS_ENV'}, function(res, textStatus) {
  //   if (res.result == 1){ window.JS_ENV.data = res.data;window.JS_ENV.loaded = true;}
  // }, "json");
});



// function naturalSort(stringArray) {
  // логическое исключающее "или"
  var xor = function(a, b) {
    return a ? !b : b;
  };
  // проверяет, является ли символ цифрой
  var isDigit = function(chr) {
    var charCode = function(ch) {
      return ch.charCodeAt(0);
    };
    var code = charCode(chr);
    return (code >= charCode('0')) && (code <= charCode('9'));
  };
  // возвращает итератор для строки
  var splitString = function(str) {
    var from = 0;            // начальный индекс для slice()
    var index = 0;            // индекс для прохода по строке
    var count = 0;            // количество уже найденных частей    
    var splitter = {};          // будущий итератор
    // аналог свойства только для чтения  
    splitter.count = function () {
      return count;
    }
    // возвращает следующую часть строки  
    splitter.next = function() {
      // если строка закончилось - вернём null
      if (index === str.length) {
        return null;
      }
      // перебираем символы до границы между нецифровыми символами и цифрами
      while(++index) {
        var currentIsDigit = isDigit(str.charAt(index - 1));  
        var nextChar = str.charAt(index);
        var currentIsLast = (index === str.length);
        // граница - если символ последний,
         // или если текущий и следующий символы разнотипные (цифра / не цифра)
        var isBorder = currentIsLast ||
          xor(currentIsDigit, isDigit(nextChar));        
        if (isBorder) {
          var part = str.slice(from, index);
          from = index;
          count++;
          return {
            IsNumber: currentIsDigit,
            Value: currentIsDigit ? Number(part) : part
          }; 
        } // end if
      } // end while
    }; // end next()  
    return splitter;
  };
  // сравнивает строки в "естественном" порядке
  var compareStrings = function(str1, str2) {
    // обычное сравнение строк или чисел
    var compare = function(a, b) {  
        return (a < b) ? -1 : (a > b) ? 1 : 0; 
    };
    // получаем итераторы для строк
    var splitter1 = splitString(str1);
    var splitter2 = splitString(str2);
    // перебираем части
    while (true) {
      var first = splitter1.next();
      var second = splitter2.next();
      // если обе части существуют ...
      if (null !== first && null !== second) {
        // части разных типов (цифры либо нецифровые символы)  
        if (xor(first.IsNumber, second.IsNumber)) {
          // цифры всегда "меньше"      
          return first.IsNumber ? -1 : 1;        
        // части одного типа можно просто сравнить
        } else {                    
          var comp = compare(first.Value, second.Value);    
          if (comp != 0) {
            return comp;
          } 
        } // end if
      // ... если же одна из строк закончилась - то она "меньше"
      } else {
        return compare(splitter1.count(), splitter2.count());
      }
    } // end while
  }

