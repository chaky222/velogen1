#map-nav {
    box-shadow: 0 0 10px rgba(0,0,0,.8);
}
#map-nav .nav-inner-wrap {
    display: none;
    padding: 15px 10px;
}
#map-nav .map-nav-header {
    background: #078fc1;
    display: block;
    padding: 10px 0 10px 30px;
    color: #fff;
    font: 14px/18px Arial, Helvetica, sans-serif;
    cursor: pointer;
}

#map-nav .nav-inner-wrap.active {}
#map-nav .add-new-mark {}
#map-nav .add-new-mark label {
    display: block;
    font: 14px/16px Aria, Helvetica, sans-serif;
}
#map-nav .add-new-mark .coordinates {}
#map-nav .add-new-mark .coordinates div {
    overflow: hidden;
    margin-top: 4px;
}
#map-nav .add-new-mark .coordinates div:first-child {
    margin-top: 0;
}
#map-nav #marker-name {
    padding: 3px 10px;
    width: 305px;
    font: 12px/14px Aria, Helvetica, sans-serif;
}
#map-nav #marker-short-desc {
    padding: 3px 10px;
    width: 305px;
    resize: none;
    height: 120px;
    font: 12px/14px Aria, Helvetica, sans-serif;
}
#map-nav .add-new-mark .coordinates input {
    float: left;
    font: 14px/16px Arial, Helvetica, sans-serif;
    padding: 3px 10px;
}
#map-nav .add-new-mark .coordinates span {
    float: right;
    text-align: right;
    font: 14px/26px Arial, Helvetica, sans-serif;
}
#map-nav .add-new-mark .fields > li {
    margin-top: 15px;
}
#map-nav .add-new-mark .fields > li:first-child {
    margin-top: 0;
}
.section.maps-section{
	padding-top: 0;
}

.maps-wrapper{
    width: 100%;
    overflow: hidden;
    position: relative;
}

.sidebar-search-panel{
    position: absolute;
    top: 0;
    left: 0;
    width:350px;
    z-index: 10;
    padding-bottom: 20px;
    background-color: rgba(255,255,255,.7);
    box-sizing: border-box;
}
.sidebar-panel-header{
    background-color: #078fc1;
    box-sizing: border-box;
}
.sidebar-panel-header__title{
    color:white;
    padding: 10px 20px;
    display: inline-block;
    font-size: 18px;
    box-sizing: border-box;
}

.search-wrapper{
    padding:20px;
    box-sizing: border-box;
}
.search-input-wrapper{
    display: inline-block;
    width: 100%;
    box-sizing: border-box;
}
.search-input-wrapper__input{
    padding:10px;
}
.search-input-wrapper__btn{
    padding:10px;
}
.search-wrapper__advanced-toggler{
    display: inline-block;
    margin-top: 10px;
    color:#00658a;
    text-decoration: underline;
    cursor:pointer;
}
.search-wrapper__advanced-toggler:hover{
    color:#00658a;
    text-decoration: none;
}


.search-result-wrapper{
    box-sizing: border-box;
}
.search-result__link + .search-result__link{
    margin-top: 15px;
}
.search-result__link{
    display:block;
    min-height: 45px;
    position: relative;
    padding-left:45px;
    box-sizing: border-box;
}
.search-result__link:hover{
    background-color: #e2e2e2;
}
.search-result__link--icon{
    display: block;
    position: absolute;
    top:0;
    left: 0;
    width:40px;
    height: 40px;
}
.search-result__link--icon.color1{
    background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACZElEQVRYR61WS07jQBCtdthlJOAEJMsZgsgNiIVhO8wJJpwAOAFwAuAEwAmALfEomROAFDOzJJyAIMEOp6i2MQpJVX8cWsoiclX161evPgp8Tvd/DdLXn+TSyn9q4d19AIC39P8SKtUrCOtD17DKybB7vwDpyxHZtu32OAQVHMP68qHdliBbja7vmhCMumOvtbpkBgi3MFcNbWyYAZS9vIDoAEIGkNH+fO/98ml+ehA1Qok2GUCcnJHTbze+LVZKbZMmdLypwwPI1J7S643nARBJ/dlpglLzBusBsVB3B9BJdkmeWvX8QdiDjcbxp4+d/gGB2Bd9BBZ4Bjr9HgVbE4Kd02va7DdT2hCuCPTWpJ8EgGpZoBQxhI2VHgvAmDrqD9HKohuAOEGRyqhhLl1PXz6YKUiluig2l7xjPvqAlwBodS+xgQwlBX/u2lQZp6wf4l9KXcs1BWdkKPQAyuUoCGFzmYbP2LF3zRMS764jgH+k1tGFXNfvA0cFuRjTdIvKdir4J39BvKZOKKdBRib0DZ5+bSwDMOXTF4B3Ky4uiBOd51Xf+yaoZ8VX2JhrutNvUUOiXWCGU6nUIfyu08ke+0Iyy1REPKTSOzDBtwPQzeX1eWCZdtwdD7QfNmfbiIqwtunIPjH4BdGPS1vy7AyUEaQw+Tgw7gBcBYn4BHPfajbq3apgEnKc6CVkx0iraVYwju4MaGebIIWBM1sVTHqbJ568rJTuA5wjv7LJq5qBAr8UFIGy0Ys3H3E9hTeOpxwAHWF8C3boeOVbseSZr1/5UhI1araG8/UAdMSYFhdMh+KW7IDqDSM26yF/SZCfAAAAAElFTkSuQmCC') center no-repeat;
}
.search-result__link--icon.color2{
    background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACiklEQVRYR61WUW7TQBDdsUX4akk+aCy1CekJyA1ITkA4Acbhn3IC2hM0/ONgTkB7AsIJyA0ISZGc8hGTv0ayh9kUSy7s7HrjWorysTOzb97MvhkQFt/K8zs3Nfc5oOgJwJ4QUJfuKHAGCFMAcfFgnV42kigpGxbKGK7qfn2z55wLAN9sj/LyUXM+PjPbUgomo+tDv4uu8yXP1mRfOJ/W1mnfxIYWQIXLcxxGECyALe37zvcdMr9DEqKYeIuwzzHHAli2hhEV6KUF5awpIL46WIwjlYESgOz2Tc2l7PkPhfghEGfSAgC69PeIs5avxJuPj0sDiNvBCQg41wR8SwFHxfNlOzglKO9YqhkWlAzErWBCWT1TBkPxqbkIlc9RWzYUl+Q3+DemEsCyPZRvWUmpk0H/8dWHiQqcvnSYkDY0ygKgEqu/5jzUPl0Cb+XLMcAGIXFpcOJy+3TdlQ14dQ+0hzM6eKLsWs2Tum4FPgJ8VPkh4ldvMe6VK4FWAzCBNOsf/IymxWAm1cRMvPeuwpOyAAYkQp81KrAdOE6G22akrAf0+y940Z9rXrahYk0ZeGDqE45+ac0C0NXTFoC1FOcXLFvBlHT2qe2FRXtd9loG5OGvo9e9zEHaBXb/apv0uBFHMy6CcSGpNhXxjNTvVAffCOCvuMgM2GmnfPc0LR+u026ljSgPbJqOygxRvKDhc2EqnpGBnRqSmXwqMKUBWDTkb5oXHRP1OZjSAKRDfDQcgSPeaJtKMysqMSCdTQ1pevOVAcgAOoXULSs764DKUbmyaVa1Sjqgcr4dve63wplV4xVjWjVh0fHuFmxWvHstQd6QN/vudinx5mHHJDj3DkAGpDkxcBASbksuA+oP3oUuMPfujakAAAAASUVORK5CYII=') center no-repeat;
}
.search-result__link--icon.color3{
    background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACeklEQVRYR61WzXkTMRCdWW7hQFIBTgW4AGyvj7DmQ66AUAGhApIKYiogVGDlwxuO/uOOOwipAHMgt+wwWsf59gszktabPdgHaUZPb948DUKNLzXfWwUUbxEgBaAUEPfLcIJf/LsiJJvQ04uZ7a9j03Ku8Jea6T7BzRkgHAV3E60JcLSw2WlwL28IAkhN3iag6f1tY7JuWFkh7PVDbHgB7Hz4FmQECBXAhva/V7Vv/pAhotncDvoacSqArpmcI+K7WMZ9+wqC90ubnUt7RABO7YTFVeDwa4JS/U5Ibf57pu7nLpnb7DAaQMdMjhPEMy1hQfRxaQej6nrX5CeI8EmPkVkQGegO8xkv9KRkRPR1YQdiO/rKxmxdLMaZeZhTBNAb5s5IREpvqej/sG9mEjhv6dgfWIwHsQAYsPzNx5m3dRl8rViNATUJ0t6BZi5l6+LN7zrgNQBO3c+lRL6W6pj8KEH4ImoHYM4aSKNK4PUAriUCssVmq2qykGsSwWd+H46jAHTMpUmQxnpfbx4cQizFiFQYNq3/klfjNfGqgmIxqWVQgSkLLCiR/hK8lsxXz7oAalvx9oDecMJ1xhd1D6zu993ey4BbfGm+pU8wmTYBgJQczuwrV07xCw4kTV5FVv4pK//Ed4EggDtzcTfQXzv5hGs2rXajiWibN/Q6SucXhMOlfW1D5QsysIsgtZdPAhMNoIYg/zD1rRD1WzDRAFwADx0jHjo++Gj19XwjBlxwSJChnm8MwCXwOaRvWNnZB6RAaWTzjWqNfEAKLp9ehJ+VtVrCq+asJcJqYHUKjnG8Ry1BRZDlUMJzYitkOI8OYCPIS0Nwu9am5BhQ/wDViTEwvc8kSgAAAABJRU5ErkJggg==') center no-repeat;
}
.search-result__link--icon.color4{
    background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAChklEQVRYR62WzXISQRDHewArm5PkoHATLlpwkTcQnkB8AvVqJSU+gckTuFYor8YnMHmCxCeQE5ReWG9ED66nbKpg2541a23B9PQMm7lOT89v/v0xrcBjjWbN1lLhU0ihrwD6oFT95niEiBNU6vROkJyF7Th2dUt+5DWa1+urJHhHli9Ea8SYQMJxZ3Ek2pKBCPDq+71edVk5L7zWxS9oRWq71wNJDSvAtpfnhC4QLEAm+9XO3Pfl6/IgwAWFY8DJxgLsz5ontPncSW/Z6OVxZ3FiMjMC6GxfAcxtfhHhh1IQaRsE7ClQdy32EQG0nQEOpo0RSa+z3rwQ3xx3L8PiJil2SK95a4EwqmBU4GDWvCBHT0zOKKafKKbGcrSGDfGMoIfrPo0A+7NGzElKAAMC0IAbyxo66g8EsOcEQArQPeZFsbSWru9ZLgQsQDVI9rjmctMxf/vAm0MwbUaU4Q8YR2xJ0et1bnxkzn0h9fpOIRCSKV7V0sGHh78mRWdS16Su+H7cvRy5AXy7P1Rp5bOlDLMPh+T7l4yIQyrbDefF81zy8p3QHgZLuRu3jPJrSxZAiKcvgF8rzr1TP5hQP3jse9uaPft6qwJ6k5JRTz7nZQCqAO2ws4g4H+JAUuZXpMQ7oq55aHuACKCbyzLZiYTfbuMO/VvWdpNeqYko9yr+joYnYiV9Nn7081QKn6jAVgnJ/HwmGA8At4Sk4eRPLbhuSdLnMM4AWVVMG6FS6rUgK1vzpRTQhx0S0lrzpQG0A1uHtA0rW/cB00HTyGYb1Ur1AdPh7OtdVb/me76JV/TplYTFg8Up2KXj3WoI/ifkVZANJePuoiU1nFsHyMqSBhdIKzE3JbtA/QUXDyEwrGrHPAAAAABJRU5ErkJggg==') center no-repeat;
}
.search-result__link--icon.color5{
    background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACUElEQVRYR61X0U1CQRCcM8FftQKxAu1AqUCtQEzkxT+1AqUC9c+IiViBWoFYgXQgVqD+avScOx7kAbd793heQgi53b252bndxaDEsoeo4wfbMNiC5cdgOXcf8LvPzwO+8Gi6+EgNa1IMbZMH1XDOA5tRe8vDDS7MNdpRWxpEAdgWNmj3VLhtSlyQoT6+0YixoQKY+/ARxAQQIoCc9tfSN5/mx6JnOmhItMkAMnTptJfGd8TKYp8gXLyZFQTg1f6LVzWsxRv3nfqdkpxOlhT7AUW5lg4gwzGNz5WAJwx4Udy3Gc74+1T0EVgIM9BCj7faFILd8fBmaI8g5LRZ1ocOdqb9wgAyX0jClP7yad0QYGCpqWN9IICVVABWopK3159uxgogrJCvxIAYhKV2RSou/uku4r06gBbVbbAaDKQ8KRauJv1uBb9npmArNQWamJw+Ggzmms94RaumxSV93OuaWFIKnFrvlSflGw6zPRSj8eqeCT7hL4hXroRaGkRkwoZFkP4hdslHy2d5AOVK8Sg+C4vL83rZ8ybsldurDLhNe0DVLnAWqLIWsGau8p4RiBMfSKp1xTaLz5mGPw5gWFwGDKJ1u9kzXLf8xkaliaighVh3DF1yl7d/iGUvysBcghQ6XwhMOoB0QX6yX9Rj1I/AJAPwr6LF6mdwpNKq9IpKDHgAMUFG3nxlADkLcsdThhWJtVIpGAsyPLKJo1qlOhBy9q3X4KWwV0p4xZhzMeBTMTkFRyvev6ZgLMjacCjhoFGPFZx/B5CzsMM/MB/SlJwC6g/t3cwhbla+VQAAAABJRU5ErkJggg==') center no-repeat;
}
.search-result__link--title{
    display: inline-block;
    width: 100%;
    color:#000;
    font:700 18px robotoC;
}
.search-result__link--descr{
    display: inline-block;
    width: 100%;
    color:#636363;
}
.search-result__link--address{
    display: inline-block;
    width: 100%;
    color:#000;

}


.search-result__total{
    display: inline-block;
    margin-top: 20px;
    width: 100%;
    padding-left: 45px;
    font: 700 18px robotoC;
    color:#000;
    box-sizing: border-box;
}
.search-result__total--text{

}
.search-result__total--cnt{

}

.map-output-wrapper{
    z-index: 5;
}
.add-mark-to-map-wrap {
	z-index: 10;
	position: absolute;
	top: 200px;
	left: 400px;
	padding: 10px;
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#18b5ef+0,2989d8+50,ffde00+51,ffde00+100&amp;0.75+1,0.75+100 */
	background: -moz-linear-gradient(top,  rgba(24,181,239,0.75) 0%, rgba(24,180,239,0.75) 1%, rgba(41,137,216,0.75) 50%, rgba(255,222,0,0.75) 51%, rgba(255,222,0,0.75) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(24,181,239,0.75)), color-stop(1%,rgba(24,180,239,0.75)), color-stop(50%,rgba(41,137,216,0.75)), color-stop(51%,rgba(255,222,0,0.75)), color-stop(100%,rgba(255,222,0,0.75))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(24,181,239,0.75) 0%,rgba(24,180,239,0.75) 1%,rgba(41,137,216,0.75) 50%,rgba(255,222,0,0.75) 51%,rgba(255,222,0,0.75) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(24,181,239,0.75) 0%,rgba(24,180,239,0.75) 1%,rgba(41,137,216,0.75) 50%,rgba(255,222,0,0.75) 51%,rgba(255,222,0,0.75) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(24,181,239,0.75) 0%,rgba(24,180,239,0.75) 1%,rgba(41,137,216,0.75) 50%,rgba(255,222,0,0.75) 51%,rgba(255,222,0,0.75) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(24,181,239,0.75) 0%,rgba(24,180,239,0.75) 1%,rgba(41,137,216,0.75) 50%,rgba(255,222,0,0.75) 51%,rgba(255,222,0,0.75) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bf18b5ef', endColorstr='#bfffde00',GradientType=0 ); /* IE6-9 */
}
.select-type-block {
	position: relative;
}

.select-type-block > ul.active {
	border-top: 1px solid #ffde00;
	border-left: 1px solid #ffde00;
	border-right: 1px solid #18b5ef;
	border-bottom: 1px solid #18b5ef;
}
.select-type-block > ul {
	position: absolute;
	background-color: rgba(255,255,255,.7);
}
.select-type-block > ul > li {
	display: none;
}
.select-type-block > ul > li:first-child {
	display: block;
}
.add-mark-to-map-wrap .fields label,
.add-mark-to-map-wrap .fields select {
	display: block;
}
.add-mark-to-map-wrap .fields > li {
	margin-bottom: 15px;
}
.add-mark-to-map-wrap .fields select {
	width: 173px;
	padding: 4px 15px;
}

footer {
	display: none;
}
/*
#velogen-map-wrap .markerLabels {
    color: #fff;
    margin-top: -22px !important;
    margin-left: -20px !important;
    width: 40px;
    text-align: center;
    font: 12px/14px Arial, Helvetica, sans-serif;
}
#velogen-map-wrap .markerLabels.black {
    color: #000;
}
#velogen-map-wrap .markerLabels.bold {
    font-weight: bold;
}
#velogen-map-wrap .markerLabels.m1 {
    margin-top: -16px !important;
    margin-left: -21px !important;
}
#velogen-map-wrap .markerLabels.m2 {
    margin-top: -22px !important;
    margin-left: -13px !important;
    font-size: 14px;
    width: 26px;
}
#velogen-map-wrap .markerLabels.m3 {
    margin-top: -24px !important;
    margin-left: -21px !important;
    color: #2a0010;
    font-size: 14px;
}
*/
#velogen-map-wrap .markerLabels {
    border-radius: 1000px;
    -moz-border-radius: 1000px;
    -webkit-border-radius: 1000px;
    overflow: hidden;
    box-shadow: 0 0 10px rgba(50,50,50,.1);
	box-sizing:border-box;
}

#velogen-map-wrap .markLabelWrap.m00,
#velogen-map-wrap .markLabelWrap.m0{
    /* border:1px double rgba(255, 255, 255, .9); */
	border:1px solid #fff;
	border-radius: 1000px;
    -moz-border-radius: 1000px;
    -webkit-border-radius: 1000px;
    overflow: hidden;
    text-align: center;
    /* margin: -15px 0 0 -15px; */
}

#velogen-map-wrap .markLabelWrap.m1,
#velogen-map-wrap .markLabelWrap.m2,
#velogen-map-wrap .markLabelWrap.m3,
#velogen-map-wrap .markLabelWrap.m4,
#velogen-map-wrap .markLabelWrap.m5,
#velogen-map-wrap .markLabelWrap.m6{
    /* border:1px double rgba(255, 255, 255, .9); */
	border:3px double rgba(255, 255, 255, .7);
	border-radius: 1000px;
    -moz-border-radius: 1000px;
    -webkit-border-radius: 1000px;
    overflow: hidden;
    text-align: center;
    /* margin: -15px 0 0 -15px; */
}


#velogen-map-wrap .markLabelWrap.m00 {
    background: rgba(0, 85, 255, .8);
    text-indent: -9999px;
    width: 7px;
    height: 7px;
}

#velogen-map-wrap .markLabelWrap.m0{
    background: rgba(0, 85, 255, .5);
    text-indent: -9999px;
    width: 12px;
    height: 12px;
}
#velogen-map-wrap .markLabelWrap.m1{
    background: rgba(0, 85, 255, .3);

    text-shadow:
            0 0 7px #1c81dd,
            0 0 7px #1c81dd,
            0 0 5px #1c81dd,
            0 0 5px #1c81dd,
            0 0 5px #1c81dd,
            0 0 1px #1c81dd,
            0 0 1px #1c81dd,
            0 0 1px #1c81dd,
            0 0 1px #000;

    font-size: 14px;
    color: #fff;
    width: 24px;
    height: 24px;
}
#velogen-map-wrap .markLabelWrap.m1 .inner {
    line-height: 25px;
}
#velogen-map-wrap .markLabelWrap.m2 {
    /*background: #ffc000;*/
    background: rgba(255, 192, 000, .3);

    text-shadow:
            0 0 10px #f7db94,
            0 0 10px #f7db94,
            0 0 7px #f7db94,
            0 0 1px #f7db94
			;

    width: 38px;
    height: 38px;
    color: #000;
    font-weight: bold;
    font-size: 16px;
}
#velogen-map-wrap .markLabelWrap.m2 .inner {
    line-height: 36px;
}

#velogen-map-wrap .markLabelWrap.m3{
    /*background: #00ff00;*/
    background: rgba(0, 255, 0, .2);

    text-shadow:
            0 0 10px #9bf698,
            0 0 10px #9bf698,
            0 0 7px #9bf698,
            0 0 1px #9bf698
			;

    width: 42px;
    height: 42px;
    color: #000;
    font-size: 16px;
    font-weight: bold;
}
#velogen-map-wrap .markLabelWrap.m3 .inner{
    line-height: 42px;
}
#velogen-map-wrap .markLabelWrap.m4{
    /*background: #00ff00;*/
    background: rgba(0, 255, 0, .2);

    text-shadow:
            0 0 10px #9bf698,
            0 0 10px #9bf698,
            0 0 7px #9bf698,
            0 0 1px #9bf698
			;

    width: 48px;
    height: 48px;
    color: #000;
    font-size: 16px;
    font-weight: bold;
}
#velogen-map-wrap .markLabelWrap.m4 .inner {
    line-height: 48px;
}
#velogen-map-wrap .markLabelWrap.m5 {
    /*background: #00ff00;*/
    background: rgba(0, 255, 0, .2);

    text-shadow:
            0 0 12px #9bf698,
            0 0 12px #9bf698,
            0 0 10px #9bf698,
            0 0 1px #9bf698
			;

    width: 60px;
    height: 60px;
    color: #000;
    font-size: 16px;
    font-weight: bold;
}
#velogen-map-wrap .markLabelWrap.m5 .inner {
    line-height: 60px;
}

#velogen-map-wrap .markLabelWrap.m6 {
    /*background: #fc00ff;*/
    background: rgba(252, 0, 255, .2);

    text-shadow:
            0 0 12px #f59af4,
            0 0 12px #f59af4,
            0 0 10px #f59af4,
            0 0 1px #f59af4;

    width: 74px;
    height: 74px;
    color: #000;
    font-size: 16px;
    font-weight: bold;
}
#velogen-map-wrap .markLabelWrap.m6 .inner {
    line-height: 74px;
}
#objects-info {}
#objects-info > div {
    margin-bottom: 5px;
    padding-bottom: 5px;
    border-bottom: 2px solid #00a2dd;
}